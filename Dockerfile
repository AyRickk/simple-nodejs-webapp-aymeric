FROM node:latest
WORKDIR /app
COPY src/ src/
EXPOSE 8080
ENV APPLICATION_INSTANCE=default
CMD ["node", "src/count-server.js"]

