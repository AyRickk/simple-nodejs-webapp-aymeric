# Simple NodeJS Webapp

The application uses an environment variable `APPLICATION_INSTANCE`.  
This variable will be used by the application to listen on a specific path (i.e. `/${application_instance}/health`).

## Launch the application:

```bash
export APPLICATION_INSTANCE=example
node src/count-server.js
```



-----------------------------------------------------------------------------------------------------------------------


# Commands used for the exam

## Docker

- First, Fork the project and clone it

- Create a Dockerfile

- Build and run the projetc
```bash
docker build -t examms .
docker run -p 8080:8080 examms
```

By going to the http://0.0.0.0:8080/ address, you will see :

{
"instance": "default",
"count": 0
}


- Push container

```bash
docker tag examms ayrick/examms
docker push ayrick/examms
```

Available on https://hub.docker.com/r/ayrick/examms


## Kubernetes

- Create files deployment.yaml and service.yaml

```bash
kubectl apply -f deployment.yaml
kubectl apply -f service.yaml
minikube service examms-service
```




# Part 2: Questions

## Q1
Microservices architecture is an architecture in which an application is separated into small independent pieces called services. For example, for an online shop application, the front end, back end and database will be in 3 different microservices, communicating with each other via their respective APIs. In a Monolithic application, everything is stuck together, like a big block where everything gets mixed up.

## Q2 : 
### Microservices :

Advantages: Flexible, easy to update, independent deployment, easy to maintain.
Disadvantages: Complicated to manage, sometimes the pieces struggle to talk to each other.

### Monolithic :

Advantages: Simplicity of development, Easier to understand and manage for beginners.
Disadvantages: Single deployment for the entire application, Difficulty integrating new technologies, everything is connected.

## Q3
In a Microservices architecture, the application must be divided according to functionality or business domains to promote the independence of services. This reduces complexity, improves maintainability and facilitates upgrades and scalability. For example, for an e-business application, we could separate the application as follows:
- Product Management Service
- User Management Service
- Shopping Cart Service
- Payment Service
- Order Service
- Delivery Service


## Q4
The CAP theorem states that a distributed system can only simultaneously achieve two out of three properties: consistency, availability and partition tolerance. In microservices, this has an impact on decisions relating to the management of data between different services.

## Q5
The CAP theorem implies trade-offs in system design. For example, favouring consistency and partition tolerance may mean sacrificing availability during network partitions.

## Q6 
Microservices can improve the evolution of the application by allowing different services to be scaled independently according to demand. For example, in e-commerce, the payment service can be scaled up during peak transaction periods without affecting other services.

## Q7
Statelessness means that each request from a client to a server must contain all the information needed to understand and execute the request. In microservices, this allows services to be more scalable and resilient.

## Q8
An API gateway acts as a single point of entry for all customers. It routes requests to the appropriate microservices, manages cross-functional issues such as authentication and can bring together the results of multiple services.

